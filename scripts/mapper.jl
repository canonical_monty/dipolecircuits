""" 
Converte um índice inteiro em um estado de uma cadeia de spins de tamanho `L`.

Compatível com o modo que QuantumOptics numera os estados possívels de um sistema
de `L` spins.
"""
function convert_state(state_idx::Integer, L::Integer)
    state::Vector{Integer} = zeros(L)

    for i in L:-1:1
        state[L - i + 1] = state_idx % 3
        state_idx = state_idx ÷ 3
    end

    return (state .+ 1)
end


""" 
Cria uma entrada no dicionário com a chave e valor dados ou anexa o valor àquele
existente se a chave já existe.
"""
function insert_or_append!(dict::Dict{K, Vector{V}}, key::K, value::V) where {K, V}
    if !haskey(dict, key)
        dict[key] = [value]
    else
        push!(dict[key], value)
    end
end


"""
Mapeia os subespaços de uma cadeia de spins de tamanho `L`, separando os estados 
segundo a carga e momento totais.
"""
function map_subspaces(L::Integer)
    subspace_map = Dict{String, Vector{Integer}}()

    for state_idx in 1:3^L
        q = 0
        p = 0

        tmp = state_idx
        for i in L:-1:1
            ## QuantumOptics interpreta o valor 1 para um estado como sendo
            ## componente z = +1, 2 como sendo z = 0 e 3 como sendo z = -1.
            local_q = -(tmp % 3) + 1
            p += local_q * (i - 1)
            q += local_q

            tmp = tmp ÷ 3
        end

        insert_or_append!(subspace_map, string("q=", q, "/p=", p), state_idx)
    end

    return subspace_map
end


using FileIO, JLD2

## Local onde serão salvos os arquivos
const ROOT_DIR = "/home/vitor/CD-Códigos/DT-Doutorado/julia/DipoleCircuits/data"


function run(L::Integer)
    map = map_subspaces(L)

    # Cria o arquivo, sobrescrevendo arquivos do mesmo nome.
    filename = string("map-L=", L, ".jld2")
    filepath = joinpath(ROOT_DIR, filename)
    close(jldopen(filepath, "w"))

    jldopen(filepath, "a+") do file
        for (key, value) in map
            file[key] = value
        end
    end
end



if !isinteractive()
    for l in 4:16
        @info "Mapeando L = $l"
        run(l)
    end
end
