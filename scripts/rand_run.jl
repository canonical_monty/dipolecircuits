using DipoleCircuits
using QuantumOptics
using CairoMakie, LaTeXStrings, ColorSchemes
using JLD2

using Logging, FileIO, Dates


## Parâmetros
#
## Tamanhos das cadeias
const MIN_L = 4
const MAX_L = 6
const SIZES = collect(MIN_L:MAX_L)

## Número de execuções do circuito
const N = 100

## Número de passos de tempo
const T = 1000

## Taxa de medição
const M_STEP = 0.05
const M_RATE = collect(0.0:M_STEP:1.0)

## Local onde serão salvos os arquivos
const ROOT_DIR = "/home/vitor/CD-Códigos/DT-Doutorado/julia/DipoleCircuits/data"


## Função auxiliar para lidar com os grupos nos arquivos JLD2.
function make_path(parts::String...)
    path = parts[1]
    for p in parts[2:end]
        path = path * "/" * p
    end

    return path
end



function run()
    # Cria o arquivo, sobrescrevendo arquivos do mesmo nome.
    filename = "state-run-" * Dates.format(now(), DateFormat("yy-mm-dd-H-M")) * ".jld2"
    filepath = joinpath(ROOT_DIR, filename)
    close(jldopen(filepath, "w"))

    # Salva os parâmetros fixos no arquivo.
    jldopen(filepath, "a+") do file
        file["parameters/T"] = T
        file["parameters/N"] = N
    end

    for L in SIZES

        # Obtem o subespaço adequado
        subspace_map = jldopen(joinpath(ROOT_DIR, "map-L=$L.jld2"), "r")
        subspace = subspace_map["q=0"]["p=0"]
        close(subspace_map)

        for R in M_RATE

            s = SimulationParameters(L, T, R, N)
            c = ProjectiveCircuit(s)
            p = ZEntropyProbe(s)


            (mag, ent) = run_random_state_simulation!(s, c, p, subspace)


            l_group = string("L=", L)
            r_group = string("R=", R)
            jldopen(filepath, "a+") do file
                file[make_path(l_group, r_group, "mag")] = mag
                file[make_path(l_group, r_group, "ent")] = ent
            end

        end
    end

end



if !isinteractive()
    run()
end
