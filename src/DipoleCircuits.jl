module DipoleCircuits
export run_simulation!, run_simulation_loop!, run_random_state_simulation!,
        # base.jl
            SimulationParameters, 
            generate_state_random,
            generate_state_from_index,
            generate_state_haar,
        # circuits.jl
            DipoleCircuit, BasicCircuit, ProjectiveCircuit,
            apply,
        # measurements.jl
            ZEntropyProbe
            # Resto



using Logging
using LinearAlgebra, Random
using MakieCore, GLMakie, LaTeXStrings, ColorSchemes

using QuantumOptics, RandomMatrices

include("utils.jl")
include("base.jl")
include("circuits.jl")
include("measurements.jl")



## Esta função poderia partir só de SimulationParamenters e talvez enuns para
## indicar o tipo de circuito e de medição a ser realizada. Seria próprio para
## usar via argumentos externos.

## Construtores
##
## Estado: Índice, Superposição?, Aleatório, Haar
##
## Circuito: Básico, Projetivo
##
## Sondas: Z, Entropia, ZEntropia, ...


function run_simulation!(s::SimulationParameters, c::DipoleCircuit, p::Probe, ψ)
    @info "Iniciando simulação: L = $(s.L)\tT = $(s.T)\tR = $(s.R)\tN = $(s.N)"
    
    B = generate_base(s)
    ϕ = basisstate(B, 1)

    for r in 1:s.N 
        @info "\tExecução #$r"

        ϕ.data = ψ.data
        @inline run_simulation_loop!(s.T, c, p, ϕ)
    end

    take_mean!(p, s.N)
    retrieve_data(p)
end


function run_random_state_simulation!(
    s::SimulationParameters,
    c::DipoleCircuit,
    p::Probe,
    subspace::Vector{Integer}
)

    @info "Iniciando simulação: L = $(s.L)\tT = $(s.T)\tR = $(s.R)\tN = $(s.N)"
    
    B = generate_base(s)
    dim = 3^(s.L)
    s_dim = length(subspace)
    M = rand(Haar(2), s_dim, 2)

    idx = 1
    for r in 1:s.N
        @info "\tExecução #$r"

        v::Vector{ComplexF64} = zeros(dim)
        @views v[subspace] .= M[:, idx]
        ψ = Ket(B, v)

        @inline run_simulation_loop!(s.T, c, p, ψ)
        
        idx += 1
        if idx == s_dim + 1
            M .= rand(Haar(2), s_dim, 2)
            idx = 1
        end
    end

    take_mean!(p, s.N)
    retrieve_data(p)
end



function run_simulation_loop!(T::Integer, c::DipoleCircuit, p::Probe, ψ)
    for t in 1:T
        @debug "\t\tPasso de tempo: T = $t"

        @inline measure!(p, t, ψ)
        @inline apply!(c, ψ)
    end
end

end # module DipoleCircuits

## Notas
#
# O estado de número 56170, para dez spins é um bom estado.

# Para L = 8:
#
#   Não congelados
#   2729
