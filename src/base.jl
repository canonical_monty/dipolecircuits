struct SimulationParameters
    L::Integer
    T::Integer
    R::Float64
    N::Integer

    function SimulationParameters(L::Integer, T::Integer, R::Float64, N::Integer)

        if L <= 0
            throw(DomainError(L, "Número de sítios deve ser maior que zero."))
        end

        if T <= 0
            throw(DomainError(T, "Número de passos de tempo deve ser maior que zero."))
        end

        if !(0 <= R <= 1.0)
            throw(DomainError(R, "Taxa de medições deve estar entre zero e um."))
        end

        if N <= 0
            throw(DomainError(N, "Número de execuções do circuito deve ser maior que zero."))
        end

        new(L, T, R, N)
    end
end



## Estados iniciais
function generate_state_random(s::SimulationParameters)
    b = SpinBasis(1)
    B = b
    for i in 2:s.L
        B = tensor(B, b)
    end

    ψ = basisstate(B, rand(1:3^(s.L)))
end

function generate_state_from_index(s::SimulationParameters, idx::Integer)

    if 0 <= idx <= 3^(s.L)
        throw(DomainError(idx, "Índice deve ser positivo e menor que $(3^(s.L))."))
    end

    b = SpinBasis(1)
    B = b
    for i in 2:s.L
        B = tensor(B, b)
    end

    ψ = basisstate(B, idx)
end


function generate_state_haar(s::SimulationParameters)
    b = SpinBasis(1)
    B = b
    for i in 2:s.L
        B = tensor(B, b)
    end

    ψ = randstate_haar(B)
end



function generate_base(s::SimulationParameters)
    b = SpinBasis(1)
    B = b
    for i in 2:s.L
        B = tensor(B, b)
    end

    B
end
