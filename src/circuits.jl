## Circuitos

""" Produz uma unitária atuando em três sítios e que conserva carga e dipolo.

A unitária tem uma estrutura bloco diagonal contendo quatro blocos dois por dois
implementando as transições permitidas. O resto dos blocos é trivial.

As transições permitidas são, na ordenação usual

    07 <-> 11       -+- <-> 0-0
    21 <-> 17       +-+ <-> 0+0
    12 <-> 08       +-0 <-> 0+-
    16 <-> 20       -+0 <-> 0-+

Os blocos implementando estas transições são selecionados segundo a distribuição
de Haar --- ensemble unitário circular.
"""
function generate_three_site_gate_matrix()

    gate = Matrix{ComplexF64}(I, 27, 27)
    U1 = rand(Haar(2), 2)
    U2 = rand(Haar(2), 2)
    U3 = rand(Haar(2), 2)
    U4 = rand(Haar(2), 2)
    
    gate[[7,11], [7, 11]] = U1
    gate[[21,17], [21, 17]] = U2
    gate[[12,8], [12, 8]] = U3
    gate[[16,20], [16,20]] = U4

    return gate
end


function generate_three_site_gate()

    b = SpinBasis(1)
    tensor_basis = tensor(b, b, b)

    M = generate_three_site_gate_matrix()
    return SparseOperator(tensor_basis, M)
end



""" Gera uma camada do aparelho de portas quânticas do circuito.

Uma camada consiste em um conjunto de unitárias três por três que preservam
a carga e o dipolo cobrindo todos os sítios da cadeia. Caso sobrem um ou dois 
sítios, a identidade atua neles.

Camadas sucessivas são deslocadas em relação umas às outras de modo a conectar 
todos os sítios do sistema.
"""
function assemble_gate_layer!(n::Integer, r::Integer, id, op)
    
    for i in 1:n
        op = tensor(op, generate_three_site_gate())
    end

    for i in 1:r
        op = tensor(op, id)
    end

    return op
end


""" Gera a unitária para um passo de tempo do circuito.

Correspondentemente, três camadas de unitárias.
"""
function generate_circuit_block(size::Integer)

    b = SpinBasis(1)
    id = identityoperator(b)

    ## Primeira camada
    first_layer = tensor(id, id)
    n = (size - 2) ÷ 3
    r = (size - 2) % 3
    first_layer = assemble_gate_layer!(n, r, id, first_layer)

    ## Segunda camada
    second_layer = id
    n = (size - 1) ÷ 3
    r = (size - 1) % 3
    second_layer = assemble_gate_layer!(n, r, id, second_layer)

    ## Terceira camada
    third_layer = generate_three_site_gate()
    n = (size - 3) ÷ 3
    r = (size - 3) % 3
    third_layer = assemble_gate_layer!(n, r, id, third_layer)


    return [first_layer, second_layer, third_layer]
end


""" Gera lista de projetores utilizados em medições projetivas. """
function generate_projectors(L::Integer)

    b = SpinBasis(1)
    B = b
    for i in 2:L
        B = tensor(B, b)
    end

    P_minus = projector(basisstate(b, 1))
    P_zero = projector(basisstate(b, 2))
    P_plus = projector(basisstate(b, 3))

    P_list = Matrix{Operator}(undef, L, 3)
    for (i, op) in enumerate([P_minus, P_zero, P_plus])
        for j in 1:L
            P_list[j, i] = embed(B, j, op)
        end
    end

    P_list
end



""" Tipo básico para circuitos.

Este tipo serve para possibilitar a especialização de circuitos. Em particular,
um que aplica apenas a evolução unitária e outro que realiza medições projetivas
também.
"""
abstract type DipoleCircuit end

function apply!(c::DipoleCircuit, ψ) 
    throw(NotImplementedError("DipoleCircuit", "apply", "$(typeof(c))"))
end




""" Circuito básico """
struct BasicCircuit <: DipoleCircuit
    L::Integer

    function BasicCircuit(s::SimulationParameters)
        new(s.L)
    end
end

function apply!(c::BasicCircuit, ψ)
    for U in generate_circuit_block(c.L)
        ψ .= U * ψ
    end

    ## return ψ
end



""" Circuito que também faz medidas """
struct ProjectiveCircuit <: DipoleCircuit
    L::Integer
    R::Float64
    P

    function ProjectiveCircuit(s::SimulationParameters)
        P = generate_projectors(s.L)
        new(s.L, s.R, P)
    end
end

    
function apply!(c::ProjectiveCircuit, ψ)
    rng = rand()
    if rng <= c.R
        S = rand(1:c.L)

        ψ_minus = c.P[S, 1] * ψ
        ψ_zero  = c.P[S, 2] * ψ
        ψ_plus  = c.P[S, 3] * ψ


        ψ_dag = dagger(ψ)
        p_minus = real(ψ_dag * ψ_minus)
        p_zero  = real(ψ_dag * ψ_zero)
        p_plus  = real(ψ_dag * ψ_plus)

        m = rand()
        if m <= p_minus
            ψ.data .= ψ_minus.data ./ sqrt(p_minus)
        elseif m <= p_minus + p_zero
            ψ.data .= ψ_zero.data ./ sqrt(p_zero)
        else
            ψ.data .= ψ_plus.data ./ sqrt(p_plus)
        end
    end

    for U in generate_circuit_block(c.L)
        ψ .= U * ψ
    end

    ## return ψ
end
