abstract type Probe end

function measure!(p::Probe, t::Integer, ψ)
    throw(NotImplementedError("Probe", "measure!", "$(typeof(p))"))
end

function take_mean!(p::Probe, n::Integer)
    throw(NotImplementedError("Probe", "take_mean!", "$(typeof(p))"))
end

function retrieve_data(p::Probe)
    throw(NotImplementedError("Probe", "retrieve_data", "$(typeof(p))"))
end



mutable struct ZProbe <: Probe
    L::Integer
    Z
    data

    function ZProbe(s::SimulationParameters)
        b = SpinBasis(1)
        Z = 0.5 * sigmaz(b)

        data = zeros(s.L, s.T) # Manter assim?
        new(s.L, Z, data)
    end
end

function measure!(p::ZProbe, t::Integer, ψ) 
    for s in 1:p.L
        p.data[s, t] += expect(s, p.Z, ψ)
    end
end

function take_mean!(p::ZProbe, n::Integer)
    p.data = p.data ./ n
end

function retrieve_data(p::ZProbe)
    p.data
end



mutable struct PZProbe <: Probe
    L::Integer
    Z
    data

    function PZProbe(s::SimulationParameters)
        b = SpinBasis(1)
        Z = 0.5 * sigmaz(b)

        data = zeros(s.L, s.T) # Manter assim?
        new(s.L, Z, data)
    end
end

function measure!(p::PZProbe, t::Integer, ψ) 
    for s in 1:p.L
        p.data[s, t] += (s - 1) * expect(s, p.Z, ψ)
    end
end

function take_mean!(p::PZProbe, n::Integer)
    p.data = p.data ./ n
end

function retrieve_data(p::PZProbe)
    p.data
end




mutable struct EntropyProbe <: Probe
    L::Integer
    data

    function EntropyProbe(s::SimulationParameters)
        data = zeros(s.L - 1, s.T) 
        new(s.L, data)
    end
end

function measure!(p::EntropyProbe, t::Integer, ψ) 
    for s in 1:(p.L - 1)
        ρ = ptrace(ψ, 1:s)

        ## O `real` é necessário porque as vezes o valor da entropia
        ## contêm uma parte complexa pequena O(e-15), que causa erro
        ## na hora de salvar o valor no vetor.
        p.data[s, t] += real(entropy_vn(ρ))
    end
end




mutable struct ZEntropyProbe <: Probe
    L::Integer
    Z
    z_data
    ent_data

    function ZEntropyProbe(s::SimulationParameters)
        b = SpinBasis(1)
        Z = 0.5 * sigmaz(b)

        z_data = zeros(s.L, s.T) 
        ent_data = zeros(s.L - 1, s.T)
        new(s.L, Z, z_data, ent_data)
    end
end


function measure!(p::ZEntropyProbe, t::Integer, ψ)
    for s in 1:(p.L - 1)
        ρ = ptrace(ψ, 1:s)
        p.ent_data[s, t] += real(entropy_vn(ρ))
        p.z_data[s, t] += expect(s, p.Z, ψ)
    end

    p.z_data[p.L, t] += expect(p.L, p.Z, ψ)
end

function take_mean!(p::ZEntropyProbe, n::Integer)
    p.z_data = p.z_data ./ n
    p.ent_data = p.ent_data ./ n
end

function retrieve_data(p::ZEntropyProbe)
    (p.z_data, p.ent_data)
end
