""" Produz índices lineares para uma base

Produz `LinearIndices` para uma base composta. Assim, é possível passar 
um índice cartesiano para o objeto retornado e obter um índice linear.

"""
function Base.LinearIndices(basis::CompositeBasis{S, B}) where {S, B}
    LinearIndices(tuple(basis.shape...))
end



""" 
Exceção usada para quando um método de uma interface não foi
implementado.
"""
struct NotImplementedError <: Exception
    interface::String
    func::String
    type::String
end

Base.showerror(io::IO, e::NotImplementedError) = begin
    printstyled(io, "NotImplementedError: ", color = :red)
    print(io, "Método `", e.func, "` faz parte da interface `", e.interface,
          "` e não implementado para tipo `", e.type, "`. Usando implementação reserva.")
end




## Plots
function map_to_color(v::Vector{T}, colors) where {T}
    l = length(v)
    L = length(colors)

    r = L ÷ l
    colorscheme = []
    for i in range(1, step = r, length = l)
        push!(colorscheme, colors[i])
    end
    
    colorscheme
end


function plot_entropy(L::Integer, samples, data, colors)

    F = Figure()
    ax = Axis(
        F[1, 1],
        xlabel = L"x", 
        xticks = 1:L,
        xlabelsize = 24,
        ylabel = L"S(x)",
        ylabelsize = 24
    )

    plots = []
    subs = []
    colorscheme = map_to_color(samples, colors)
    for (s, c) in zip(samples, colorscheme)
        push!(plots, scatterlines!(ax, 1:L - 1, data[:, s], color = c))
        push!(subs, "t = $s")
    end


    Label(
        F[0, 1],
        text = L"\text{Entropia de emaranhamento}",
        fontsize = 24,
        tellwidth = false
    )

    Legend(F[1,2], plots, subs)

    F
end


function plot_magnetization_profiles(
    L::Integer,
    T::Integer,
    data,
    colormap = :magma,
    #colorrange = MakieCore.Automatic,
)

    F = Figure()

    ## Mapa de calor
    xs = 1:L
    ts = 1:(T + 1)

    ax = Axis(
        F[1, 1],
        xlabel = L"x",
        xticks = 1:L,
        xlabelsize = 24,
        ylabel = L"t",
        ylabelsize = 24
    )

    ## Mapa de calor
    hm = heatmap!(ax, xs, ts, data, colormap = colormap)#, colorrange = colorrange)

    ## Barra de cor
    Colorbar(F[1, 2], hm, ticks = -1.0:0.25:1.0)

    ## Títulos
    Label(
        F[0, 1],
        text = L"\text{Perfil de magnetização}",
        fontsize = 24,
        tellwidth = false,      # Impede que a coluna diminua para o tamanho do título
    )

    Label(
        F[0, 2],
        text = L"\langle S_{x}(t) \rangle",
        fontsize = 20,
        padding = (15, 0, 0, 0)
    )


    F
end


